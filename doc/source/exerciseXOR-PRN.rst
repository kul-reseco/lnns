Exercise: XOR problem using a *pattern* file and the CLI
========================================================

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/lnns/issues?status=new&status=open>`_.

Tutorial Data Set
-----------------

We will use a special file format, the *pattern* file (.prn), which is a text file of the following form::

    number_of_patterns: 4
    number_of_inputs: 2
    number_of_outputs: 1

    0 0 2
    0 1 1
    1 0 1
    1 1 2

We need two files, one for training, and one for predicting:

 - *xor.prn* for training,
 - *xor_grid.prn* for predicting.

Goal
----
We will solve the XOR problem with the MLP Classifier. We can use special pattern files (.prn) for this purpose,
however this is only possible in the CLI, as these are text files and not images.

In order to do this, we will need a Neural Network with 3 layers: an input layer with 2 input neurons, a hidden
layer with 2 neurons and an output layer with one neuron.

.. figure:: images/xor_neural_network.PNG
   :width: 50%

We will however not do a normal classification.
Instead, we will calculate the probability for each input that the output is class 1!
Have another look at the training data, we have two classes: class 1 and class 2.

Run the CLI
-----------

We need the following input for the CLI:

 - the path to the file we want to predict
 - the path to the training file
 - [-n] the no-data-value is -1 (default)
 - [-l] the number of neurons per hidden layer --> 2 neurons in 1 hidden layer
 - [-t] the test size is 0
 - [-p] the class we want the probability for: class 1
 - [-o] the output file path (optional)

We keep the default values for the activation function (logistic) and the number of iterations (200).

The resulting command should look at the least something like::

    mlpclassifier-pattern "C:/.../xor_grid.prn" "C:/.../xor.prn" -l 2 -t 0 -p 1

You will have two output files: the prediction of the patterns and a graph of the error decline.

You can plot the results using R, Excel, matplotlib, pyqtgraph, ...
See `the QGIS exercise <exerciseXOR-QGIS.html>`_ for a discussion of the results.

