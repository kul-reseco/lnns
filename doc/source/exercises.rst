Exercises
=========

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/lnns/issues?status=new&status=open>`_.


We have developed tree exercises. Two solving the XOR problem (one with the command line, and one with QGIS) and
one solving an actual image classification problem.

You can find the data (*test_data.zip*) on the `bitbucket page <https://bitbucket.org/kul-reseco/lnns/downloads/>`_.

.. toctree::
   :maxdepth: 1

   exerciseXOR-QGIS
   exerciseXOR-PRN
   exerciseIMG-QGIS

