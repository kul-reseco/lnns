Neural Network MLPClassifier Documentation
==========================================

.. figure:: images/xor_neural_network.PNG
   :width: 50%

.. toctree::
   :hidden:

   context
   installation
   userguide
   exercises
   reference

.. include:: ../../README.md

.. image:: images/lumos_big.svg
   :width: 40 %

.. image:: images/logo.png
   :width: 20 %

Indexes
-------

* :ref:`genindex`
* :ref:`modindex`

