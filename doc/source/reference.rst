Neural Network MLPClassifier API
================================

Source code: https://bitbucket.org/kul-reseco/lnns/src.

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/lnns/issues?status=new&status=open>`_.

Algorithms
----------

.. automodule:: lnns.core.network
    :members:
    :undoc-members:
    :show-inheritance:

Interfaces
------------

.. automodule:: lnns.interfaces
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: lnns.interfaces.imports
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: lnns.interfaces.exports
    :members:
    :undoc-members:
    :show-inheritance:


.. automodule:: lnns.interfaces.lnns_gui
    :members:
    :undoc-members:
    :show-inheritance:


CLI: mlpclassifier-image
------------------------

.. argparse::
   :module: lnns.interfaces.network_image_cli
   :func: create_parser
   :prog: mlpclassifier-image

CLI: mlpclassifier-pattern
--------------------------

.. argparse::
   :module: lnns.interfaces.network_prn_cli
   :func: create_parser
   :prog: mlpclassifier-pattern



