User Guide
==========

The Neural Network MLPClassifier can be used in several ways:

    1. As a plugin in QGIS
    2. From the QGIS processing toolbox
    3. As a commandline interface to classify images
    4. As a commandline interface to classify *pattern* files
    5. Adapting the code to fulfil very specific needs

For the last option, we refer the user to the `code repository <https://bitbucket.org/kul-reseco/lnns/src>`_
and the `API at the end of this document <reference.html>`_.

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/lnns/issues?status=new&status=open>`_.

QGIS Plugin
-----------

.. figure:: images/gui.PNG
   :width: 70%

Required settings:

#. Select the image, or multiple images in case bands are saved in separate images, that you want to classify.
   At least two bands are required.

#. Select the raster with training pixels. Be aware of value that represents pixels with no training data
   (*no-data-value*).

#. Set the no-data-value.

Optional settings:

- Choose the number of hidden layers and number of neurons per layer, as a comma separated list.

- Choose another activation function:

  - IDENTITY: no-op activation, useful to implement linear bottleneck, returns f(x) = x;
  - LOGISTIC: logistic sigmoid function, returns f(x) = 1 / (1 + exp(-x));
  - TANH: hyperbolic tan function, returns f(x) = tanh(x);
  - RELU: rectified linear unit function, returns f(x) = max(0, x).

- Choose another number of iterations for training the neural network.

- Set a different test size (the portion of training pixels that will be used to evaluate the trained network).

- Instead of classifying the image, we can predict the probability for each image pixel of finding a given class.

- Choose a filename for the output layer(s).


Command Line Interface for image classification
-----------------------------------------------

The main command is::

        >mlpclassifier-image

Use :code:`-h` or :code:`--help` to list all possible arguments::

        >mlpclassifier-image -h

The **images_folder**, **image_names** and **classes_data_name** are required arguments. Notice how there are
**no spaces** between the different band images, in case your file or folder names contain spaces, use double quotes.
An example::

        >mlpclassifier-image folder/to/data band1.tif,band2.tif,band3.tif training_data.tif

By default, the classified image is stored in the same folder as the image files, with the name 'output_classified.tif'.
To select another file name base (no extension) or another location, use the argument :code:`-o` or :code:`--output`::

        >mlpclassifier-image folder/to/data band1.tif,band2.tif,band3.tif training_data.tif -o folder/to/output.tif

To select a no-data-value for the training data set, use the argument :code:`-n` or :code:`--no_data_value`. The
default no-data-value is -1::

        >mlpclassifier-image folder/to/data band1.tif,band2.tif,band3.tif training_data.tif -n 0

To select a different activation function for the MLP classifier (identity, logistic=default, tanh, relu),
use the argument :code:`-a` or :code:`--activation`::

        >mlpclassifier-image folder/to/data band1.tif,band2.tif,band3.tif training_data.tif -a identity

To select a different number of iterations (default 200), use the argument :code:`-i` or :code:`--iterations`::

        >mlpclassifier-image folder/to/data band1.tif,band2.tif,band3.tif training_data.tif -i 2000

To select a different number of hidden layers and their neurons, use the argument :code:`-l` or
:code:`--hidden_layer_size`::

        >mlpclassifier-image folder/to/data band1.tif,band2.tif,band3.tif training_data.tif -l 5,5

To select a different test size (between 0 and 1) for evaluating the network training, use the argument :code:`-t` or
:code:`--test_size`::

        >mlpclassifier-image folder/to/data band1.tif,band2.tif,band3.tif training_data.tif -t 0.50

If, instead of classifying the image, you would like to predict the probability for each image pixel of finding a given
class, use the argument :code:`-p` or :code:`--probability_of_class`::

        >mlpclassifier-image folder/to/data band1.tif,band2.tif,band3.tif training_data.tif -p 4


Command Line Interface for pattern file classification
------------------------------------------------------

The main command is::

        >mlpclassifier-pattern

Use :code:`-h` or :code:`--help` to list all possible arguments::

        >mlpclassifier-pattern -h

The **pattern_predict_path** and **pattern_train_path** are required arguments. Use double quotes around paths and
file names in case they contain spaces. The default **no-data-value** is -1, change this with the argument
:code:`-n` or :code:`--no_data_value` to change. An example::

        >mlpclassifier-pattern data/folder/predict.prn data/folder/train.prn -n 0

The other options are the same as with *mlpclassifier-image*.

