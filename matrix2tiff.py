import numpy as np
import os
from osgeo import gdal

# a = np.arange(0, 1.01, 0.01)
# b = np.ones((101, 101))
# c = np.around(a * b, decimals=2)
# d = np.transpose(c)
#
# file_name = os.path.join(os.path.dirname(os.path.realpath(__file__)), "xor.tiff")
# driver = gdal.GetDriverByName('GTiff')
# raster = driver.Create(file_name, 101, 101, 2, gdal.GDT_Float32)
# raster.GetRasterBand(1).WriteArray(d)
# raster.GetRasterBand(2).WriteArray(c)
# raster.SetGeoTransform((0, 0.01, 0, 0, 0, 0.01))
# raster.SetProjection('PROJCS["Belge 1972 / Belgian Lambert 72",GEOGCS["Belge 1972",'
#                      'DATUM["Reseau_National_Belge_1972",SPHEROID["International 1924",6378388,297,AUTHORITY["EPSG",'
#                      '"7022"]],TOWGS84[-99.059,53.322,-112.486,0.419,-0.83,1.885,-1],AUTHORITY["EPSG","6313"]],'
#                      'PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,'
#                      'AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4313"]],PROJECTION["Lambert_Conformal_Conic_2SP"],'
#                      'PARAMETER["latitude_of_origin",90],PARAMETER["central_meridian",4.36748666666667],'
#                      'PARAMETER["standard_parallel_1",51.1666672333333],PARAMETER["standard_parallel_2",49.8333339],'
#                      'PARAMETER["false_easting",150000.013],PARAMETER["false_northing",5400088.438],UNIT["metre",1,'
#                      'AUTHORITY["EPSG","9001"]],AXIS["Easting",EAST],AXIS["Northing",NORTH],AUTHORITY["EPSG",'
#                      '"31370"]]')
# raster.FlushCache()
# raster = None

a = np.zeros((101, 101))
a = a - 1
a[0, 0] = 1
a[0, 100] = 2
a[100, 0] = 2
a[100, 100] = 1

file_name = os.path.join(os.path.dirname(os.path.realpath(__file__)), "xor_training.tiff")
driver = gdal.GetDriverByName('GTiff')
raster = driver.Create(file_name, 101, 101, 1, gdal.GDT_Float32)
raster.GetRasterBand(1).WriteArray(a)
raster.GetRasterBand(1).SetNoDataValue(-1)
raster.SetGeoTransform((0, 0.01, 0, 0, 0, 0.01))
raster.SetProjection('PROJCS["Belge 1972 / Belgian Lambert 72",GEOGCS["Belge 1972",'
                     'DATUM["Reseau_National_Belge_1972",SPHEROID["International 1924",6378388,297,AUTHORITY["EPSG",'
                     '"7022"]],TOWGS84[-99.059,53.322,-112.486,0.419,-0.83,1.885,-1],AUTHORITY["EPSG","6313"]],'
                     'PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,'
                     'AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4313"]],PROJECTION["Lambert_Conformal_Conic_2SP"],'
                     'PARAMETER["latitude_of_origin",90],PARAMETER["central_meridian",4.36748666666667],'
                     'PARAMETER["standard_parallel_1",51.1666672333333],PARAMETER["standard_parallel_2",49.8333339],'
                     'PARAMETER["false_easting",150000.013],PARAMETER["false_northing",5400088.438],UNIT["metre",1,'
                     'AUTHORITY["EPSG","9001"]],AXIS["Easting",EAST],AXIS["Northing",NORTH],AUTHORITY["EPSG",'
                     '"31370"]]')
raster.FlushCache()
raster = None
