# -*- coding: UTF-8 -*-.
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : April 2020
| Copyright           : © 2018 - 2020 by Tinne Cahy (Geo Solutions) and Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the QGIS Neural Network MLP Classifier plugin and mlp-image-classifier python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import unittest
from qgis.core import QgsApplication
app = QgsApplication([], True)
app.initQgis()


class ExtendedUnitTesting(unittest.TestCase):

    @staticmethod
    def clean_up(clean_up_files):
        for file in clean_up_files:
            if os.path.exists(file):
                try:
                    os.remove(file)
                except PermissionError:
                    pass

    @staticmethod
    def append_with_aux_xml(files):
        return ['{}.aux.xml'.format(x) for x in files]
