<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" maxScale="0" styleCategories="AllStyleCategories" version="3.4.14-Madeira" minScale="1e+08">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="paletted" alphaBand="-1" opacity="1" band="1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <colorPalette>
        <paletteEntry label="1 water" alpha="255" color="#b0dcdb" value="1"/>
        <paletteEntry label="2 mangrove /aquacultuur" alpha="255" color="#cc5c7a" value="2"/>
        <paletteEntry label="3 urbaan / naakte gond" alpha="255" color="#efeda2" value="3"/>
        <paletteEntry label="4 landbouw" alpha="255" color="#e49953" value="4"/>
        <paletteEntry label="5 bos" alpha="255" color="#46963d" value="5"/>
        <paletteEntry label="6 ontbost / bergtop / mijnbouw" alpha="255" color="#dedec9" value="6"/>
        <paletteEntry label="0 no data" alpha="0" color="#000000" value="0"/>
      </colorPalette>
      <colorramp type="randomcolors" name="[source]"/>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" grayscaleMode="0" saturation="0" colorizeStrength="100" colorizeBlue="128" colorizeRed="255"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
