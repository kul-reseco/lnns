# -*- coding: UTF-8 -*-.
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : August 2018
| Copyright           : © 2018 - 2020 by Tinne Cahy (Geo Solutions) and Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the QGIS Neural Network MLP Classifier plugin and mlp-image-classifier python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
from os.path import join, dirname, realpath
import numpy as np
from osgeo import gdal

from lnns.interfaces.imports import import_image
from lnns.interfaces.exports import write_classified_image
from lnns.core.network import Network
from tests import ExtendedUnitTesting

xor_folder = join(dirname(realpath(__file__)), "xor")
satellites_folder = join(dirname(realpath(__file__)), "satellite")


class TestLNNSImages(ExtendedUnitTesting):

    def test_xor_image_exercise(self):
        # input
        band_data, metadata = import_image(join(xor_folder, 'xor.tiff'), False)
        classes_data, _ = import_image(join(xor_folder, 'xor_training.tiff'))

        # create network
        network_2_3_2 = Network(number_of_hidden=(3,), activation='logistic')

        # train network using the input
        network_2_3_2.train_image(band_data=band_data, classes_data=classes_data, max_iter=200, no_data_value=-1,
                                  test_size=0)

        # predict classification using trained network
        classified_probability_image = network_2_3_2.predict_image(band_data, probability_of_class=1)

        # in case you would like to visualize the results in QGIS
        output = join(xor_folder, "temp.tiff")
        write_classified_image(file_path=output, image=classified_probability_image, gdal_type=gdal.GDT_Float64,
                               geo_transform=metadata['geo_transform'], projection=metadata['projection'])

        # Coordinate (0,0) is left under in QGIS. But in our MATRIX band_data it is [0, 0]
        # probability that (0, 0) = 0 is ~ 100%
        self.assertLess(classified_probability_image[0, 0], 0.01)
        # probability that (1, 1) = 0 is ~ 100%
        self.assertLess(classified_probability_image[100, 100], 0.01)
        # probability that (0, 1) = 0 is ~ 0% because it should be 1
        self.assertGreater(classified_probability_image[0, 100], 0.99)
        # probability that (1, 0) = 0 is ~ 0% because it should be 1
        self.assertGreater(classified_probability_image[100, 0], 0.99)
        # probability that (0.5, 0.5) = 0 should be in between
        self.assertGreater(classified_probability_image[50, 50], 0.99)

        # clean up
        clean_up_paths = [join(xor_folder, 'xor.tiff'), join(xor_folder, 'xor_training.tiff'), output]
        clean_up_files = self.append_with_aux_xml(clean_up_paths)
        clean_up_files.append(output)
        self.clean_up(clean_up_files)

    def test_satellite_exercise(self):
        # input
        image_1, _ = import_image(join(satellites_folder, 'WINDOWED_SPOT_XI_VIETNAM_011.tif'), True)
        image_2, _ = import_image(join(satellites_folder, 'WINDOWED_SPOT_XI_VIETNAM_012.tif'), True)
        image_3, _ = import_image(join(satellites_folder, 'WINDOWED_SPOT_XI_VIETNAM_013.tif'), True)
        image_4, _ = import_image(join(satellites_folder, 'WINDOWED_SPOT_XI_VIETNAM_014.tif'), True)
        band_data = np.concatenate([image_1, image_2, image_3, image_4])
        classes_data, metadata = import_image(join(satellites_folder, 'reference.tif'))

        # create network
        network_4_10_6 = Network(number_of_hidden=(10,), activation='logistic')
        # train network using the input
        network_4_10_6.train_image(band_data=band_data, classes_data=classes_data, max_iter=2000)

        # evaluate
        validation_results = network_4_10_6.validation_results
        self.assertGreater(validation_results['average_accuracy'], 0.92)
        self.assertGreater(validation_results['kappa_class_1'], 0.95)
        self.assertGreater(validation_results['kappa_class_2'], 0.94)
        self.assertGreater(validation_results['kappa_class_3'], 0.87)
        self.assertGreater(validation_results['kappa_class_4'], 0.88)
        self.assertGreater(validation_results['kappa_class_5'], 0.93)
        self.assertGreater(validation_results['kappa_class_6'], 0.82)
        self.assertGreater(validation_results['average_kappa'], 0.9)
        self.assertIn('precision    recall  f1-score   support', validation_results['report'])

        # predict classification using trained network
        classified_image = network_4_10_6.predict_image(band_data=band_data)

        # evaluate: allow max 10 % difference - usually around 5 %
        result, _ = import_image(join(satellites_folder, 'output_for_comparison.tif'))
        difference_pct = np.count_nonzero(result[0, :, :] - classified_image) / classified_image.size * 100
        self.assertLess(difference_pct, 10)
        print("{:.2f} percent of the raster cells are different from the comparison file. \n".format(difference_pct))

        # clean up
        image_names = ['WINDOWED_SPOT_XI_VIETNAM_011.tif', 'WINDOWED_SPOT_XI_VIETNAM_012.tif',
                       'WINDOWED_SPOT_XI_VIETNAM_013.tif', 'WINDOWED_SPOT_XI_VIETNAM_014.tif', 'reference.tif']
        clean_up_paths = [join(satellites_folder, image) for image in image_names]
        clean_up_files = self.append_with_aux_xml(clean_up_paths)
        self.clean_up(clean_up_files)
