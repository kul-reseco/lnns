# -*- coding: UTF-8 -*-.
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : August 2018
| Copyright           : © 2018 - 2020 by Tinne Cahy (Geo Solutions) and Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the QGIS Neural Network MLP Classifier plugin and mlp-image-classifier python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
from os.path import join, dirname, abspath
import numpy as np
from qgis.PyQt.QtCore import QTimer
from lnns.interfaces.network_image_cli import create_parser as image_parser, run_network as image_run
from lnns.interfaces.network_prn_cli import create_parser as prn_parser, run_network as prn_run
from lnns.interfaces.imports import import_image, read_pattern
from tests import ExtendedUnitTesting, app


# examples
# mlpclassifier-prn "C:/.../xor.prn" "C:/.../xor_grid.prn
# mlpclassifier-image "C:/.../satellite" WINDOWED_SPOT_XI_VIETNAM_011.rst,WINDOWED_SPOT_XI_VIETNAM_012.rst,
#                                        WINDOWED_SPOT_XI_VIETNAM_013.rst,WINDOWED_SPOT_XI_VIETNAM_014.rst
#                                        referentiedata.rst -i 2000


class TestLNNSCLI(ExtendedUnitTesting):

    def test_lnns_satellite_cli(self):
        folder = join(dirname(abspath(__file__)), "satellite")
        image_names = ["WINDOWED_SPOT_XI_VIETNAM_011_012.tif", "WINDOWED_SPOT_XI_VIETNAM_013.tif",
                       "WINDOWED_SPOT_XI_VIETNAM_014.tif"]
        classes_data_name = "reference.tif"
        output = join(folder, "temp_cli.tiff")

        QTimer.singleShot(2000, app.closeAllWindows)
        parser = image_parser()
        args = [folder, ",".join(image_names), classes_data_name, '-i=2000', '-n=0', '-t=0.33', '-o={}'.format(output)]
        image_run(parser.parse_args(args))

        # evaluate: allow max 10 % difference - usually around 5 %
        result, _ = import_image(output)
        compare, _ = import_image(join(folder, 'output_for_comparison.tif'))
        difference_pct = np.count_nonzero(result - compare) / result.size * 100
        self.assertLess(difference_pct, 10)
        print("{:.2f} percent of the raster cells are different from the comparison file. \n".format(difference_pct))

        # clean up
        clean_up_paths = [join(folder, image) for image in image_names + [classes_data_name]] + [output]
        clean_up_files = self.append_with_aux_xml(clean_up_paths)
        clean_up_files.append(join(folder, "output_error.PNG"))
        clean_up_files.append(output)
        self.clean_up(clean_up_files)

    def test_lnns_xor_image_cli(self):
        folder = join(dirname(abspath(__file__)), "xor")
        images = ["xor.tiff"]
        classes_data_name = "xor_training.tiff"
        output = join(folder, "temp_xor_cli.tiff")

        QTimer.singleShot(2000, app.closeAllWindows)
        parser = image_parser()
        args = [folder, ",".join(images), classes_data_name, '-l=3', '-n=-1', '-t=0', '-p=1', '-o={}'.format(output)]
        image_run(parser.parse_args(args))

        result, _ = import_image(output)
        self.assertLess(result[0][0, 0], 0.01)
        self.assertLess(result[0][100, 100], 0.01)
        self.assertGreater(result[0][0, 100], 0.99)
        self.assertGreater(result[0][100, 0], 0.99)
        self.assertGreater(result[0][50, 50], 0.99)

        # clean up
        clean_up_paths = [join(folder, image) for image in images + [classes_data_name]] + [output]
        clean_up_files = self.append_with_aux_xml(clean_up_paths)
        clean_up_files.append(output)
        clean_up_files.append(join(folder, "output_error.PNG"))
        self.clean_up(clean_up_files)

    def test_lnns_xor_pattern_cli(self):
        folder = join(dirname(abspath(__file__)), "xor")
        output = join(folder, "temp_xor_cli.prn")
        path_prn_predict = join(folder, "xor_grid.prn")
        path_prn_train = join(folder, "xor.prn")

        QTimer.singleShot(2000, app.closeAllWindows)
        parser = prn_parser()
        args = [path_prn_predict, path_prn_train, '-l=3', '-n=-1', '-t=0', '-p=1', '-o={}'.format(output)]
        prn_run(parser.parse_args(args))

        result = read_pattern(output)
        self.assertLess(result.y[0][0], 0.015)
        self.assertLess(result.y[0][10199], 0.015)
        self.assertGreater(result.y[0][99], 0.985)
        self.assertGreater(result.y[0][10099], 0.985)
        self.assertGreater(result.y[0][5099], 0.985)

        self.clean_up([output])
        self.clean_up([join(folder, "xor_grid_error.PNG")])
