# -*- coding: UTF-8 -*-.
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : February 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven) and Tinne Cahy (Geo Solutions)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the QGIS Neural Network MLP Classifier plugin and mlp-image-classifier python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
from os.path import join, abspath, dirname, basename
import numpy as np
from qgis.core import QgsRasterLayer, QgsProject
from qgis.PyQt.QtCore import QTimer
from lnns.interfaces.lnns_gui import NeuralNetworkWidget
from lnns.interfaces.imports import import_image
from tests import ExtendedUnitTesting, app


class TestLNNSGUI(ExtendedUnitTesting):

    def test_app_opens(self):

        z = NeuralNetworkWidget()
        z.show()

        QTimer.singleShot(2000, app.closeAllWindows)
        app.exec_()

    def test_app_runs(self):
        folder = join(dirname(abspath(__file__)),  "satellite")
        output = join(folder, "temp.tiff")

        image_paths = [join(folder, "WINDOWED_SPOT_XI_VIETNAM_011_012.tif"),
                       join(folder, "WINDOWED_SPOT_XI_VIETNAM_013.tif"),
                       join(folder, "WINDOWED_SPOT_XI_VIETNAM_014.tif")]
        ref_path = join(folder, "reference.tif")

        for path in image_paths:
            layer = QgsRasterLayer(path, basename(path), 'gdal')
            QgsProject.instance().addMapLayer(layer, True)

        layer = QgsRasterLayer(ref_path, basename(ref_path), 'gdal')
        QgsProject.instance().addMapLayer(layer, True)
        z = NeuralNetworkWidget()

        # filling out items
        items = [z.listWidget.item(x) for x in [0, 1, 2]]
        [item.setSelected(True) for item in items]
        z.classifiedComboBox.setLayer(layer)
        z.outputFileWidget.lineEdit().setText(output)

        # clicking OK
        QTimer.singleShot(2000, app.closeAllWindows)
        z._run_lnns()

        # evaluate: allow max 10 % difference - usually around 5 %
        result, _ = import_image(output)
        compare, _ = import_image(join(folder, 'output_for_comparison.tif'))
        difference_pct = np.count_nonzero(result - compare) / result.size * 100
        self.assertLess(difference_pct, 10)
        print("{:.2f} percent of the raster cells are different from the comparison file. \n".format(difference_pct))

        # clean up
        QgsProject.instance().removeAllMapLayers()
        clean_up_files = self.append_with_aux_xml(image_paths + [output])
        clean_up_files.append(output)
        self.clean_up(clean_up_files)
