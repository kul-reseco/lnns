# -*- coding: UTF-8 -*-.
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : August 2018
| Copyright           : © 2018 - 2020 by Tinne Cahy (Geo Solutions) and Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the QGIS Neural Network MLP Classifier plugin and mlp-image-classifier python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import numpy as np
from lnns.core.network import Network
from tests import ExtendedUnitTesting


class TestLNNSPattern(ExtendedUnitTesting):

    def test_pattern(self):
        # TRAINING
        x_train = np.array([[0, 0], [0, 1], [1, 0], [1, 1]], dtype=np.double)
        y_train = np.array([0, 1, 1, 0])

        xor_net = Network(number_of_hidden=(3,), activation='logistic')
        xor_net.network.max_iter = 200
        xor_net.network.fit(x_train, y_train)
        # plot(range(len(xor_net.network.loss_curve_)), xor_net.network.loss_curve_, testing=True)
        self.assertLess(xor_net.network.loss_, 0.02)

        # PREDICTION
        a = np.arange(0, 1.01, 0.01)
        x_predict = np.stack((np.repeat(a, 101), np.tile(a, 101)), axis=-1)
        y_predict = xor_net.network.predict_proba(x_predict)
        # plot(x_predict[:, 0], x_predict[:, 1], size=y_predict[:, 1], testing=True)

        # ASSERTIONS
        # point close to 0 0 ~ 0 -> >95% class 0
        self.assertGreater(y_predict[[0], [0]][0], 0.95)
        self.assertGreater(1, y_predict[[0], [0]][0])
        # point close 1 1 ~ 0 -> >95% class 0
        self.assertGreater(y_predict[[8784], [0]][0], 0.95)
        self.assertGreater(1, y_predict[[8784], [0]][0])
        # point close 0 1 ~ 1 -> <5% class 0
        self.assertGreater(y_predict[[500], [0]][0], 0)
        self.assertGreater(0.05, y_predict[[500], [0]][0])
        # point close 0.5 0.5 ~ 1 -> <5% class 0
        self.assertGreater(y_predict[[5000], [0]][0], 0)
        self.assertGreater(0.05, y_predict[[5000], [0]][0])
        # point close 1 0 ~ 1 -> <5% class 0
        self.assertGreater(y_predict[[10000], [0]][0], 0)
        self.assertGreater(0.05, y_predict[[10000], [0]][0])
