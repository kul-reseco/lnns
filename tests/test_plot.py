# -*- coding: UTF-8 -*-.
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Tinne Cahy (Geo Solutions) and Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the QGIS Neural Network MLP Classifier plugin and mlp-image-classifier python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
from os.path import join, dirname, realpath, exists
from lnns.interfaces import plot_to_file
from tests import ExtendedUnitTesting


class TestPlot(ExtendedUnitTesting):

    def test_plot(self):
        path = join(dirname(realpath(__file__)), 'test.png')
        self.clean_up([path])
        plot_to_file(x=[0, 1, 2, 3], y=[0, 1, 2, 3], path=path)
        self.assertTrue(exists(path))
        self.clean_up([path])
